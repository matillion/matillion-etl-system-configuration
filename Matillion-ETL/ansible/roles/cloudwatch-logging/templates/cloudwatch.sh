#!/bin/bash
# download and run the setup script
yum install -y https://amazoncloudwatch-agent.s3.amazonaws.com/amazon_linux/amd64/latest/amazon-cloudwatch-agent.rpm

# setup the config file
cat <<EOF > /opt/aws/amazon-cloudwatch-agent/etc/amazon-cloudwatch-agent.json
{
	"agent": {
		"run_as_user": "root"
	},
	"logs": {
		"force_flush_interval": 5,
		"logs_collected": {
			"files": {
				"collect_list": [
					{
						"file_path": "/var/log/tomcat/catalina.out",
						"log_group_name": "Matillion-ETL",
						"timestamp_format": "%d-%b-%Y %H:%M:%S.%f",
						"log_group_class": "STANDARD",
						"log_stream_name": "{instance_id}",
						"retention_in_days": 30
					}
				]
			}
		}
	}
}
EOF

systemctl enable amazon-cloudwatch-agent.service
systemctl start amazon-cloudwatch-agent.service