---

# installs Java17, either from the repositories (preferred) or by direct download from Oracle if unavailable
# we'd like to know where it is to configure SystemCtl with it, so $JAVA_HOME is put in /tmp/java_home_location

- name: Ensure Java 8 is removed
  ansible.builtin.yum:
    name: java-1.8.0-openjdk-*
    state: absent

# matches both java-17-openjdk and java-17-amazon-corretto (which is OpenJDK)
- name: Install Java 17 from repositories
  when: not use_java_repository
  ansible.builtin.yum:
    name:
      - java*-17-*
    install_weak_deps: false
    lock_timeout: 180
  ignore_errors: true
  register: java_17_repository_result

- name: Record where Java17 is installed to
  when: java_17_repository_result is succeeded
  shell: echo $JAVA_HOME > /tmp/java_home_location

- name: install Java 17 manually if repository install failed
  when: use_java_repository or java_17_repository_result is failed
  block:
    - name: download Java 17 from Oracle
      ansible.builtin.get_url:
        url: https://download.oracle.com/java/17/archive/jdk-17.0.12_linux-x64_bin.tar.gz
        dest: /opt
        timeout: 60

    - name: extract Java 17 download
      ansible.builtin.unarchive:
        src: /opt/jdk-17_linux-x64_bin.tar.gz
        dest: /opt/

    - name: remove original download file
      ansible.builtin.file:
        path: /opt/jdk-17_linux-x64_bin.tar.gz
        state: absent

    - name: set the Java home to the downloaded version of Java
      shell:
        echo "export JAVA_HOME=/opt/$( ls /opt | grep jdk | head --lines 1 )" >> /etc/profile.d/java.sh

    - name: set the path to include our newly-downloaded Java
      shell:
        echo 'export PATH=$PATH:$JAVA_HOME/bin' >> /etc/profile.d/java.sh

    - name: make sure Java profile entry is executable
      shell:
        chmod +x /etc/profile.d/java.sh

    - name: Record where Java17 is installed to
      shell:
        echo /opt/$( ls /opt | grep jdk | head --lines 1 ) > /tmp/java_home_location
    
    - name: Remove docs,headless openjdk
      ansible.builtin.yum:
        name: java-17-openjdk-*
        state: absent

- name: install Tomcat Native, if available
  ansible.builtin.yum:
    name:
      - tomcat-native-1.2.*
    install_weak_deps: false
  # not always available
  ignore_errors: true
