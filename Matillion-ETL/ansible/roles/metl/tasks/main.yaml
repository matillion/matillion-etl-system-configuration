---

- name: check for SystemD
  ansible.builtin.stat:
    path: /run/systemd/system
  register: systemd_install

- name: Create Matillion Repo File
  vars:
    REPO_VER: "1.1"
    BASE_URL: "https://artifacts.matillion.com/rpm"
  ansible.builtin.yum_repository:
    file: matillion_repos
    name: "{{ item.description }}"
    description: "{{ item.description }}"
    baseurl: "{{ item.baseurl }}"
    gpgcheck: "{{ item.gpgcheck }}"
    gpgkey: "{{ item.gpgkey }}"
    enabled: "{{ item.enabled }}"
  become: true
  with_items:
    - { description: 'MatillionUniversalRPM-Stable',
        baseurl: '{{ BASE_URL }}/{{ REPO_VER }}/matillion-metl/stable/',
        gpgcheck: 'true',
        gpgkey: "{{ BASE_URL }}/{{ REPO_VER }}/matillion-metl/stable/matillion.key",
        enabled: 'yes' }
    - { description: 'MatillionCDataRPM-Stable',
        baseurl: '{{ BASE_URL }}/{{ REPO_VER }}/matillion-cdata/stable/',
        gpgcheck: 'true',
        gpgkey: "{{ BASE_URL }}/{{ REPO_VER }}/matillion-cdata/stable/matillion.key",
        enabled: 'yes' }
    - { description: 'MatillionUniversalRPM-Preview',
        baseurl: '{{ BASE_URL }}/{{ REPO_VER }}/matillion-metl/preview/',
        gpgcheck: 'false',
        gpgkey: "{{ BASE_URL }}/{{ REPO_VER }}/matillion-metl/preview/matillion.key",
        enabled: 'no' }
    - { description: 'MatillionCDataRPM-Preview',
        baseurl: '{{ BASE_URL }}/{{ REPO_VER }}/matillion-cdata/preview/',
        gpgcheck: 'false',
        gpgkey: "{{ BASE_URL }}/{{ REPO_VER }}/matillion-cdata/preview/matillion.key",
        enabled: 'no' }
    - { description: 'MatillionUniversalRPM-LTS',
        baseurl: '{{ BASE_URL }}/{{ REPO_VER }}/matillion-metl/lts/',
        gpgcheck: 'true',
        gpgkey: "{{ BASE_URL }}/{{ REPO_VER }}/matillion-metl/lts/matillion.key",
        enabled: 'yes' }
    - { description: 'MatillionCDataRPM-LTS',
        baseurl: '{{ BASE_URL }}/{{ REPO_VER }}/matillion-cdata/lts/',
        gpgcheck: 'true',
        gpgkey: "{{ BASE_URL }}/{{ REPO_VER }}/matillion-cdata/lts/matillion.key",
        enabled: 'yes' }
    - { description: 'MatillionUniversalRPM-ReleaseCandidate',
        baseurl: '{{ BASE_URL }}/{{ REPO_VER }}/matillion-metl/releasecandidate/',
        gpgcheck: 'false',
        gpgkey: "{{ BASE_URL }}/{{ REPO_VER }}/matillion-metl/releasecandidate/matillion.key",
        enabled: 'no' }
    - { description: 'MatillionCDataRPM-ReleaseCandidate',
        baseurl: '{{ BASE_URL }}/{{ REPO_VER }}/matillion-cdata/releasecandidate/',
        gpgcheck: 'false',
        gpgkey: "{{ BASE_URL }}/{{ REPO_VER }}/matillion-cdata/releasecandidate/matillion.key",
        enabled: 'no' }

- name: Check if systemctl exists
  stat:
    path: /bin/systemctl
  register: systemctl_exists

- block:
    - name: Backup /bin/systemctl
      command: mv /bin/systemctl /bin/systemctl.bak
      register: backup_systemctl
      failed_when: backup_systemctl.rc != 0
      when: systemd_install.stat.exists and systemctl_exists.stat.exists

    - name: Replace /bin/systemctl with a symlink to echo
      command: ln -s $(which echo) /bin/systemctl
      args:
        creates: /bin/systemctl
      register: replace_systemctl
      ignore_errors: true
      when: backup_systemctl.changed


  when: systemd_install.stat.exists and systemctl_exists.stat.exists
  rescue:
    - name: Restore /bin/systemctl if backup failed
      ansible.builtin.shell: mv /bin/systemctl.bak /bin/systemctl --force && chmod +x /bin/systemctl
      when: backup_systemctl.changed or replace_systemctl.changed
      register: rollback_systemctl_completed
    - debug:
        msg: "Restored /bin/systemctl due to failure in backup process"

# our 'ensure script' runs after upgrades, and it tries to restart Tomcat when it's done.
# that's good *on upgrades only*, but we don't want it here - need to copy some templates which are in the RPM first

- name: Install Matillion Universal (latest published)
  ansible.builtin.yum:
    name: matillion-*
    state: latest
    lock_timeout: 180
  become: true
  when: METL_VERSION is undefined or METL_VERSION | length == 0 or METL_VERSION == "latest"

- name: Create Matillion Repo File (specific version)
  when: METL_VERSION is defined and METL_VERSION | length > 0 and METL_VERSION != "latest"
  become: true
  block:
    - name: enable all METL repositories
      ansible.builtin.yum_repository:
        file: matillion_dev_repos
        name: "{{ item.description }}"
        description: "{{ item.description }}"
        baseurl: "{{ item.baseurl }}"
        gpgcheck: "{{ item.gpgcheck }}"
        enabled: "{{ item.enabled }}"
      with_items:
        - { description: 'MatillionUniversalRPM-Development',
            baseurl: '{{ METL_UNIVERSAL_REPO }}',
            gpgcheck: 'false',
            enabled: 'yes' }
        - { description: 'MatillionCDataRPM-Development',
            baseurl: '{{ METL_CDATA_REPO }}',
            gpgcheck: 'false',
            enabled: 'yes' }

    - name: Install Matillion Universal (J17 Specific Version)
      ansible.builtin.yum:
        name: matillion-universal-{{ METL_VERSION }}
        lock_timeout: 180

    - name: Ensure dev repo is removed
      ansible.builtin.file:
        path: "/etc/yum.repos.d/{{ item }}"
        state: absent
      with_items:
        - matillion_dev_repos.repo

- name: Update premission /bin/systemctl for excute
  ansible.builtin.shell: mv /bin/systemctl.bak /bin/systemctl --force && chmod +x /bin/systemctl
  when: replace_systemctl.changed
  ignore_errors: true

- name: Copy tomcat config files needed for METL
  vars:
    templates:
      - "context"
      - "server"
      - "tomcat-users"
  include_tasks: copy-templates.yaml
  loop: "{{ templates }}"

- name: Create temp Emerald.properties
  ansible.builtin.file:
    path: /tmp/Emerald.properties
    state: touch

- name: Create temp gcp-bigquery properties
  ansible.builtin.file:
    path: /tmp/gcp-bigquery-emerald.properties
    state: touch

- name: Create temp gcp-bigquery properties
  ansible.builtin.file:
    path: /tmp/aws-redshift-emerald.properties
    state: touch

- name: Adding working files directory to Emerald.properties
  ansible.builtin.lineinfile:
    path: /usr/share/emerald/WEB-INF/classes/Emerald.properties
    line: "TOMCAT_WORKING_DIR={{ TOMCAT_WORKING_DIR }}"
    insertbefore: EOF

- name: Adding Context.xml reference to Emerald.properties
  ansible.builtin.lineinfile:
    path: /usr/share/emerald/WEB-INF/classes/Emerald.properties
    line: "TOMCAT_CONTEXT_FILE_PATH={{ CATALINA_CONF_HOME }}/context.xml"
    insertbefore: EOF

- name: Adding Server.xml reference to Emerald.properties
  ansible.builtin.lineinfile:
    path: /usr/share/emerald/WEB-INF/classes/Emerald.properties
    line: "SERVER_XML_PATH={{ CATALINA_CONF_HOME }}/server.xml"
    insertbefore: EOF

- name: Add METL environment variables from reference file
  ansible.builtin.blockinfile:
    path: /usr/share/emerald/WEB-INF/classes/Emerald.properties
    block: "{{ lookup('file', METL_ENVIRONMENT_VARIABLES) }}"
    owner: tomcat
    group: tomcat
    mode: 0644
    create: true