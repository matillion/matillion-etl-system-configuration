<p align="center">
  <img src="./img/green-horizontal-logo-matillion.webp" alt="Matillion ETL Logo" width="650">
</p>

<br/>

## Matillion ETL Background

Matillion ETL is cloud native tool with all data jobs running in your cloud environment for maximum speed, scalability, and efficiency, optimizing both resource and economic consumption.

Matillion ETL natively supports all major cloud data warehouses and data lakes, including Snowflake, Delta Lake on Databricks, Amazon Redshift, Microsoft Azure Synapse, and Google BigQuery, to leverage the full power and benefits of each cloud platform.

<br/>

## What is in this repository?

This repository holds the information and tasks required to create a Matillion ETL node. Ansible is to assert the configuration however any
other system configuration methodology or tool can be used (including manually provisioning), if the tasks achieve the same outcome.

The following operating systems major versions are supported by Matillion ETL:

*  Centos 7
*  Centos 8 Stream
*  Redhat Enterprise Linux 7
*  Redhat Enterprise Linux 8
*  Amazon Linux 2

<br/>

## Who Uses this repository?

1. Matillion uses this configuration to build publishing machine images for each of the Cloud Marketplaces and for distribution
2. Third Parties are able to use this centralised configuration store to provision images either in the custom configuration or by developing out the tasks that have already been oultined.

*  Note: Only Matillion Billing Hub or BYOL license types can utilise this methodology. However, any type can use it as a point of reference.

<br/>

## How It Works & Usage
A remote orchestration script `orchestrate-install.sh` is pulled and then executed on the machine with two supporting property files using ansible. 

The two properties files allow the installation to be configured in two ways:

*  Specify the extent of the install e.g. minimal, enhanced or otherwise (Appendix 1) `/tmp/installation.vars`
*  Specify what environment variables to pass to the Matillion Application (Appendix 2) `/tmp/Emerald.properties`

Please review Appendix 1 and 2 to construct these supporting files for the installation, they can be re-used upon every Matillion ETL installation.

Once these two files are available on the remote machine, run:
```
curl https://bitbucket.org/matillion/matillion-etl-system-configuration/raw/stable/orchestrate-install.sh > /tmp/orchestrate-install.sh
chmod a+x /tmp/orchestrate-install.sh
/bin/bash /tmp/orchestrate-install.sh /tmp/installation.vars
```

Once the installation is complete, reboot the machine either via `reboot` or in the relevant cloud provider console and in a few minutes the Matillion ETL service should be available on port 80. 

You can then login using:
matillion-user / matillion-password

As soon as possible, set the authentication up in a more secure manner/changing passwords etc.

<br/>
<br/>
<hr/>

## Appendicies

### Appendix 1: Installation Variables
This section explains how to construct a properties file which dictates how the system configuration acts during the install.

An example of this, along with an explanation of each of the properties is shown below:

*  `CONFIGURATION_MANAGEMENT_TOOL`: Currently only ansible is supported, others may come in future versions
*  `CONFIGURATION_MANAGEMENT_REPO`: The configuration management repository name 
*  `CONFIGURATION_MANAGEMENT_BRANCH`: The branch name or commit of the installation instructions (`stable` being the most up to date)
*  `METL_ENVIRONMENT_VARIABLES`: The METL Environment Variables file defined in Appendix 2

<br/>

An example of this in practice:
```
---
# --------------------------------------------------------------------
CONFIGURATION_MANAGEMENT_TOOL: ansible
CONFIGURATION_MANAGEMENT_REPO: https://bitbucket.org/matillion/matillion-etl-system-configuration.git
CONFIGURATION_MANAGEMENT_BRANCH: stable
# --------------------------------------------------------------------
METL_ENVIRONMENT_VARIABLES: "/tmp/<file-referenced-in-appendix-2>" # usually /tmp/Emerald.properties
# --------------------------------------------------------------------
```
#### Futher Install Configuration Options
An example file of <strong>all</strong> the possible properties can be found here:
https://bitbucket.org/matillion/matillion-etl-system-configuration/raw/stable/Matillion-ETL/ansible/group_vars/installationservers

This file looks like:
```
---
# Matillion ETL Installation & Configuration Example .vars file
# --------------------------------------------------------------------
...
# Base OS Further Configuration
MINIMAL: true
HARDEN: true
ENHANCE: true
AUTO_SECURITY_UPDATES: true
...
```

For example, if there was a requirement for a minimal install (without any supporting CLIs or otherwise), the following properties could be set which would run the minimal install on the remote machine:

```
---
# Matillion ETL Installation & Configuration Example .vars file
# --------------------------------------------------------------------
...
# Base OS Further Configuration
MINIMAL: true
HARDEN: false
ENHANCE: false
AUTO_SECURITY_UPDATES: false
...
```

<hr/>

<strong>When the file is constructed, move it into `/tmp/installation.vars` on the remote machine</strong>

<hr/>

### Appendix 2: Emerald.properties file
This section explains how to construct a properties file which allows the installation to pass environment variables through to the Matillion ETL installation, these are passed in using a reference file which needs to be on the machine prior to install. 

This file is then placed into the `/usr/share/emerald/WEB-INF/classes/Emerald.properties` location to be referenced by the running Matilllion ETL application.

A guide on how to construct this properties file is shown below, by appending the relevant blocks to an empty file:
```
                                                Create Blank File
                                                        |
                            ____________byol___________/ \___billing-platform________
                            |                          \ /                          |
                            |                      Billing Type?                    |
                    # Billing-based Configuration:                    # Billing based Configuration:
                    LICENSE_SOURCE=byol                               SEC_ENABLE_SAAS_BILLING=true
                            |                                         BILLING_API_BASE_URL=https://api.billing.matillion.com
                            |                                         BILLING_URL=https://billing.matillion.com
                            |                      Cloud Platform?                  |
                            |___________________________/ \_________________________|        
                                                        \ /
                                                         |
                            ____________________________/|\__________________________
                            |                            |                          |
                          aws                           gcp                       azure
                            |                            |                          |
                            |                            |                          |
                    # Platform Configuration:         # Platform Configuration:     # Platform Configuration:
                    PLATFORM_TYPE=aws                 PLATFORM_TYPE=gcp             PLATFORM_TYPE=azure
                            |                            |                          |
                            |                            |                          |
                            |___________________________/ \_________________________|        
                                                        \ /
                                                   Datawarehouse?
                          ______________________________/|\____________________________
                          |             |                |             |              |
                      snowflake      redshift         bigquery      synapse       deltalake
                          |_____________|________________|_____________|______________|
                                                         |
                                                 # Datawarehouse Configuration:
                                                 DATABASE_ENVIRONMENT=<value above>
                                                         |
                                                         |
                                                  Persistence Type?
                                                        / \       
                                __On Board (default)____\ /_____External___________     
                                |                                                 |
            # Persistence-based Configuration:                    # Persistence-based Configuration:
            PERSISTENCE_PASSWORD_POSTGRES=postgres                PERSISTENCE_PASSWORD_POSTGRES=postgres
            PERSISTENCE_USERNAME_POSTGRES=postgres                PERSISTENCE_USERNAME_POSTGRES=postgres
            PERSISTENCE_STORE_NAME=postgres                       PERSISTENCE_STORE_NAME=postgres
                                |                                 PERSISTENCE_URL_POSTGRES='jdbc:postgresql://34.54.23.53:5432/matilliondatabase'
                                |                                                 |                                      
                                |___________Able to add further properties________|
                                             if they are needed/required
```

<hr/>

Example properties file for AWS Snowflake BYOL, using default local postgres persistence:
```
# Billing-based Configuration:
LICENSE_SOURCE=byol

# Platform Configuration:
PLATFORM_TYPE=aws

# Datawarehouse Configuration:
DATABASE_ENVIRONMENT=snowflake

# Persistence-based Configuration:
PERSISTENCE_PASSWORD_POSTGRES=postgres
PERSISTENCE_USERNAME_POSTGRES=postgres
PERSISTENCE_STORE_NAME=postgres
```
<hr/>

Example properties file for GCP Bigquery Billing-Hub Enabled, using remote postgres persistence:
```
# Billing-based Configuration:
SEC_ENABLE_SAAS_BILLING=true
BILLING_API_BASE_URL=https://api.billing.matillion.com
BILLING_URL=https://billing.matillion.com

# Platform Configuration:
PLATFORM_TYPE=gcp

# Datawarehouse Configuration:
DATABASE_ENVIRONMENT=bigquery

# Persistence-based Configuration:
PERSISTENCE_PASSWORD_POSTGRES=my-password
PERSISTENCE_USERNAME_POSTGRES=my-username
PERSISTENCE_URL_POSTGRES="jdbc:postgresql://34.54.23.53:5432/matilliondatabase"
PERSISTENCE_STORE_NAME=postgres
```
<hr/>

<strong>When the file is constructed, move it into `/tmp/Emerald.properties` on the remote machine</strong>

<hr/>

### Appendix 3: Installing Manually
This section explains how to install without using the orchestration wrapper; `orchestrate-install.sh` by using totally manual steps. This should be used if:

*  Ansible is not permitted on the remote machine/as part of the installation
*  The installation environment is strict and no external calls are permitted (e.g. `yum install wget` using remote mirrors)

Manually fulfil the following ansible roles (and the relevant included tasks) manually:

*  OS-Minimal
*  Tomcat
*  Postgres (if using onboard persistence)
*  Matillion-ETL

e.g. For the yum installs, manually install the following packages using internal mirrors or similar:
```
- name: Install YUM Dependencies
  yum:
    name: "{{ yum_packages }}"
    enablerepo: "epel"
  vars:
    yum_packages:
      - java-1.8.0-openjdk-headless-1.8.0.*
      - java-1.8.0-openjdk-devel-1.8.0.*
      - tomcat-native-1.2.23-1*
  become: true
```

*  If the installation <strong>should</strong> work on the remote machine but there is a bug awaiting to be fixed in the orchestration wrapper, this is an option that can be utilised.

<br/>
<hr/>

## Matillion Internal Notes:
### Making Development Changes to this README.md

*  The easiest method to make active changes to this readme is to install grip (https://pypi.org/project/grip/).
*  Then in the repository home, run `grip 80` and you can view your active changes in localhost:80 in your browser.

### Testing ansible-lint locally prior to push
*  In the root of the repository run `./ci-functions/ansible-lint.sh`

### Making support of version changes to dependencies
*  If changes are required to the OS minimal dependencies or similar, these need to be sycned across all the supported OS'es (VM + Containerised), namely:
  *  CentOS 7.*
  *  CentOS 8.* Stream
  *  RHEL 7.*
  *  RHEL 8.*
  *  Amazon Linux 2.*
