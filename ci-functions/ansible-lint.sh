#!/bin/bash

set -eo pipefail

tidy_up() {
    rm -rf ./results/
}

analyse_results() {

    echo -e "\n\n--------------------------------------------------------------------------------------"
    echo "Ansible Linting Analysis Results"
    echo -e "--------------------------------------------------------------------------------------"

    if ls ./results/linting/ | grep failed | xargs >/dev/null 2>&1 ; then files_present=$(ls ./results/linting/ | grep failed | xargs) ; else files_present="None Found" ; fi
    if ls ./results/linting/failed* >/dev/null 2>&1 ; then failed_linting_number=$(ls ./results/linting/failed* | wc -l) ; else failed_linting_number="0" ; fi

    echo -e "*******************************************"
    echo "Failed Linting: $failed_linting_number"
    echo -e "*******************************************"

    if (( $failed_linting_number > 0 )); then
     
      echo -e "\n--------------------------------------------------------------------------------------"
      echo -e "LINTING FAILURES BREAKDOWN BY FILE:"
      echo -e "--------------------------------------------------------------------------------------"
        
      # Loop Through and push out Linting Failure Details
      for file in $(ls ./results/linting/ | grep failed | xargs)
      do 
        file_path_parsed=$(echo $file | sed 's%\^%/%g')
        file_path=$(echo "./$file" | sed 's%\./failed-%%g')
        echo -e "\n*******************************************"
        echo -e "Results for $file_path_parsed"
        echo -e "*******************************************"
        cat ./results/linting/$file_path
        echo -e "*******************************************"
      done

      echo -e "Please amend the above issues and resubmit the build."
      echo -e "--------------------------------------------------------------------------------------"

      tidy_up

      exit 1

    else

      echo -e "--------------------------------------------------------------------------------------"
      echo -e "Policy and Linting requirements met, testing successful."
      echo -e "--------------------------------------------------------------------------------------"
      tidy_up
      exit 0

    fi

}

ansible_validate() {

    file_path=$1
    parsed_file_path=$(echo $file_path | sed 's%/%^%g' | sed 's/^..//')
    file_name=$(echo "${file_path##*/}")

    echo -e "\n-------------------------------------------"
    echo -e "Validating file against Ansible Lint"
    echo -e "File Path: $file_path"
    (ansible-lint $file_path &> ./results/linting/$parsed_file_path && echo "Result: Meets Criteria") || (touch ./results/linting/failed-$parsed_file_path && echo "Result: Failed Criteria")
    echo -e "-------------------------------------------"
    
}

main() {
  
  mkdir -p ./results/linting/

  ansible_files=$(find . -name '*.yaml' | grep main | xargs) 
  
    echo -e "\n-------------------------------------------"
    echo -e "Checking the linting of the following files:"
    find . -name '*.yaml' | grep main
    echo -e "-------------------------------------------"

  for file_path in $ansible_files 
  do 

    ansible_validate $file_path
    
  done

  analyse_results

}

main $1