#!/bin/bash

set -x

DOCKER_IMAGE="$1"
NOCACHE="$RANDOM$RANDOM$RANDOM"

curl https://bitbucket.org/matillion/matillion-etl-system-configuration/raw/$CIRCLE_BRANCH/orchestrate-install.sh?ignore=$NOCACHE --output ./orchestrate-install.sh$NOCACHE

cat > validation-environment.vars$NOCACHE << EOF
DATABASE_ENVIRONMENT=snowflake
PLATFORM_TYPE=aws
PERSISTENCE_PASSWORD_POSTGRES=postgres
PERSISTENCE_STORE_NAME=postgres
PERSISTENCE_USERNAME_POSTGRES=postgres
TOMCAT_RESTART_COMMAND=/opt/tomcat/bin/catalina.sh stop -force ; sleep 30 ; /usr/share/tomcat/bin/catalina.sh start
EOF

cat > metl-install.vars$NOCACHE << EOF
CONFIGURATION_MANAGEMENT_BRANCH: $CIRCLE_BRANCH
CONFIGURATION_MANAGEMENT_REPO: https://bitbucket.org/matillion/matillion-etl-system-configuration.git
CONFIGURATION_MANAGEMENT_TOOL: ansible
METL_ENVIRONMENT_VARIABLES: /tmp/validation-environment.vars
PYTHON_VERSIONS: 
EOF

cat > Dockerfile << EOF
# create a docker image with METL on it
FROM $DOCKER_IMAGE
COPY ./orchestrate-install.sh$NOCACHE /tmp/orchestrate-install.sh$NOCACHE
COPY ./metl-install.vars$NOCACHE /tmp/metl-install.vars$NOCACHE
COPY ./validation-environment.vars$NOCACHE /tmp/validation-environment.vars
RUN  /bin/bash /tmp/orchestrate-install.sh$NOCACHE /tmp/metl-install.vars$NOCACHE

# validate that what we've installed actually starts up
COPY ./validate-startup.sh /tmp/validate-startup$NOCACHE.sh
RUN  /bin/bash /tmp/validate-startup$NOCACHE.sh
EOF

docker image build --no-cache --progress=plain .
