#!/bin/bash
# -------------------------------------------------------------
# This CI build validates a build against a dockerised agent
# via a docker compose file against the local branch.
# Call like: ./ci-functions/docker-validate.sh al2-install
# -------------------------------------------------------------

# Any failure should fail this CI build
set -euo pipefail

docker_compose_container=$1

echo -e "\nCONFIGURATION_MANAGEMENT_BRANCH: $CIRCLE_BRANCH" >> ./ci-functions/packer-jobs/core-build.vars
echo "DOCKERISED_INSTALL: true" >> ./ci-functions/packer-jobs/core-build.vars
docker-compose down || echo "INFO: None to tear down"
docker-compose up -d
docker exec -i $docker_compose_container /orchestrate-install.sh /core-build.vars
