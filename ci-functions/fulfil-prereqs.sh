#!/bin/bash

set -eo pipefail

main() {
	STAGE=$1
	case "$STAGE" in
		ansible_lint) 
			apt-get update
			pip3 install "ansible-lint" ansible
			;;

		packer_validate) 
			echo "----- INSTALLING PACKER ----------------"
			cd /tmp/
			export PACKER_RELEASE="1.9.4"
			wget -q https://releases.hashicorp.com/packer/${PACKER_RELEASE}/packer_${PACKER_RELEASE}_linux_amd64.zip
			unzip packer_${PACKER_RELEASE}_linux_amd64.zip
			sudo mv packer /usr/local/bin
			echo "----- SETTING GCP PERMISSIONS ----------------"
			echo "${GOOGLE_CREDS}" >> /tmp/gcp-credentials
			mkdir /tmp/bin/
			curl https://sdk.cloud.google.com > ./install.sh
			chmod a+x ./install.sh
			./install.sh --disable-prompts --install-dir=/tmp/bin/
			export PATH=$PATH:/tmp/bin/google-cloud-sdk/bin/
			gcloud auth activate-service-account --key-file=/tmp/gcp-credentials
			gcloud config set project ${MATILLION_GCP_PROJECT}
			# Installl Azure CLI
			curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash
			az login --service-principal -u $AZURE_ACCESS_KEY -p $AZURE_SECRET_KEY --tenant $AZURE_TENANT
			echo "----- INSTALL PIP FOR AWS CLI AUTH CALLS ----------------"
			sudo apt-get update
			sudo apt-get install python pip
			;;

		*) 
			echo "STAGE was not passed or is not recognised. Value passed: ($STAGE)"
			exit 1
			;;
	esac
}

check_for_inputs() {
	STAGE=$1
	[[ -z $STAGE ]] && ( echo "No Stage Handed, Exiting" && exit 1)
	return 0
}

check_for_inputs $1

main $1
