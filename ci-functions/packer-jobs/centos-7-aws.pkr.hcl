//-------------------------------------------------------------------
// These are the variables are used for the packer definition
//-------------------------------------------------------------------

variable "matillion_project_id" {
  type    = string
  default = "${env("MATILLION_GCP_PROJECT")}"
}

variable "date" {
  type    = string
  default = "${env("DATE")}"
}

variable "authtoken" {
  type    = string
  default = "${env("AUTH_TOKEN")}"
}

variable "accounttoken" {
  type    = string
  default = "${env("ACCOUNT_TOKEN")}"
}

variable "scanendpoint" {
  type    = string
  default = "${env("SCAN_ENDPOINT")}"
}

variable "cloudprovider" {
  type    = string
  default = "aws"
}

variable "slackendpoint" {
  type    = string
  default = "${env("SLACK_ENDPOINT")}"
}

//-------------------------------------------------------------------

source "amazon-ebs" "validation" {
  ami_name                    = "metl-sysconfig-${var.date}-centos7"
  associate_public_ip_address = "true"
  instance_type               = "t3.large"
  region                      = "us-east-1"
  skip_create_ami             = "true"
  skip_region_validation      = "false"
  source_ami                  = "ami-02451607de984ab25" # Updated to the publishing account AMI (same config as ami-01ff43c6ac5beacf9 in matillion-legacy us-east-1)
  ssh_pty                     = "true"
  ssh_username                = "centos"
  subnet_id                   = "subnet-06e9fb09"
  vpc_id                      = "vpc-f1394d8b"
  launch_block_device_mappings {
    device_name           = "/dev/sda1"
    delete_on_termination = true
    volume_type           = "gp3"
  }
  tags = {
    ci-validation = "true"
  }
}

build {
  sources = [
    "source.amazon-ebs.validation", 
    ]

  // Using a wrapper script rather than copying the files straight across
  // as is more accurately simulates customer behaviour for the install
  // orchestration.
  provisioner "file" {
    source = "./orchestrate-install.sh"
    destination = "/tmp/orchestrate-install.sh"
  }

  provisioner "file" {
    source = "./ci-functions/packer-jobs/core-build.vars"
    destination = "/tmp/core-build.vars"
  }

  provisioner "file" {
    source = "./ci-functions/packer-jobs/validation-environment.vars"
    destination = "/tmp/validation-environment.vars"
  }

  provisioner "shell" {
    inline = ["chmod a+x /tmp/orchestrate-install.sh && sudo /bin/bash /tmp/orchestrate-install.sh /tmp/core-build.vars"]
  }

  post-processor "manifest" {
    output     = "manifest.json"
    strip_path = true
  }
}
