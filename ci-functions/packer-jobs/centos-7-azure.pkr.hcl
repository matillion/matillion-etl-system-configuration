//-------------------------------------------------------------------
// These are the variables are used for the packer definition
//-------------------------------------------------------------------

variable "date" {
  type    = string
  default = "${env("DATE")}"
}

variable "azure_access_key" {
  type    = string
  default = "${env("AZURE_ACCESS_KEY")}"
}

variable "azure_secret_key" {
  type    = string
  default = "${env("AZURE_SECRET_KEY")}"
}

variable "azure_subscription_id" {
  type    = string
  default = "${env("AZURE_SUBSCRIPTION_ID")}"
}

variable "authtoken" {
  type    = string
  default = "${env("AUTH_TOKEN")}"
}

variable "accounttoken" {
  type    = string
  default = "${env("ACCOUNT_TOKEN")}"
}

variable "scanendpoint" {
  type    = string
  default = "${env("SCAN_ENDPOINT")}"
}

variable "cloudprovider" {
  type    = string
  default = "azure"
}

variable "slackendpoint" {
  type    = string
  default = "${env("SLACK_ENDPOINT")}"
}

//-------------------------------------------------------------------

source "azure-arm" "validation" {
  capture_container_name = "images"
  image_offer            = "CentOS"
  image_publisher        = "OpenLogic"
  image_sku              = "7.7"
  location               = "centralus"
  os_disk_size_gb        = "40"
  os_type                = "Linux"
  resource_group_name    = "metlsysvalidation"
  ssh_pty                = "true"
  ssh_username           = "centos"
  storage_account        = "metlsysvalidation"
  vm_size                = "Standard_DS2_v2"
  client_id              = "${var.azure_access_key}"
  client_secret          = "${var.azure_secret_key}"
  subscription_id        = "${var.azure_subscription_id}"
  capture_name_prefix    = "${var.date}"

  azure_tags = {
    category             = "Development"
    owner                = "development@matillion.com"
    sbu                  = "Pod 1"
  }
}

build {
  sources = [
    "source.azure-arm.validation", 
  ]

  // Using a wrapper script rather than copying the files straight across
  // as is more accurately simulates customer behaviour for the install
  // orchestration.
  provisioner "file" {
    source = "./orchestrate-install.sh"
    destination = "/tmp/orchestrate-install.sh"
  }

  provisioner "file" {
    source = "./ci-functions/packer-jobs/core-build.vars"
    destination = "/tmp/core-build.vars"
  }

  provisioner "file" {
    source = "./ci-functions/packer-jobs/validation-environment.vars"
    destination = "/tmp/validation-environment.vars"
  }

  provisioner "shell" {
    inline = ["chmod a+x /tmp/orchestrate-install.sh && sudo /bin/bash /tmp/orchestrate-install.sh /tmp/core-build.vars"]
  }

  post-processor "manifest" {
    output     = "manifest.json"
    strip_path = true
  }
}
