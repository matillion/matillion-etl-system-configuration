//-------------------------------------------------------------------
// These are the variables are used for the packer definition
//-------------------------------------------------------------------

variable "matillion_project_id" {
  type    = string
  default = "${env("MATILLION_GCP_PROJECT")}"
}

variable "date" {
  type    = string
  default = "${env("DATE")}"
}

variable "authtoken" {
  type    = string
  default = "${env("AUTH_TOKEN")}"
}

variable "accounttoken" {
  type    = string
  default = "${env("ACCOUNT_TOKEN")}"
}

variable "scanendpoint" {
  type    = string
  default = "${env("SCAN_ENDPOINT")}"
}

variable "cloudprovider" {
  type    = string
  default = "gcp"
}

variable "slackendpoint" {
  type    = string
  default = "${env("SLACK_ENDPOINT")}"
}

//-------------------------------------------------------------------

source "googlecompute" "validation" {
  disk_size    = "30"
  account_file = "/tmp/gcp-credentials"
  instance_name = "metl-sysconfig-${var.date}-centos7"
  source_image = "centos-7-v20220126"
  image_name   = "metl-sysconfig-${var.date}"
  machine_type = "n1-standard-2"
  metadata = {
    enable-oslogin = "false"
  }
  project_id   = "${var.matillion_project_id}"
  ssh_pty      = "true"
  skip_create_image = "true"
  ssh_username = "packer"
  zone         = "europe-west1-b"
  scopes = [
    "https://www.googleapis.com/auth/cloud-platform"
  ]
  labels = {
    "purpose" = "ci-validation"
  }
}

build {
  sources = [
    "source.googlecompute.validation", 
    ]

  // Using a wrapper script rather than copying the files straight across
  // as is more accurately simulates customer behaviour for the install
  // orchestration.
  provisioner "file" {
    source = "./orchestrate-install.sh"
    destination = "/tmp/orchestrate-install.sh"
  }

  provisioner "file" {
    source = "./ci-functions/packer-jobs/core-build.vars"
    destination = "/tmp/core-build.vars"
  }

  provisioner "file" {
    source = "./ci-functions/packer-jobs/validation-environment.vars"
    destination = "/tmp/validation-environment.vars"
  }

  provisioner "shell" {
    inline = ["chmod a+x /tmp/orchestrate-install.sh && sudo /bin/bash /tmp/orchestrate-install.sh /tmp/core-build.vars"]
  }

  post-processor "manifest" {
    output     = "manifest.json"
    strip_path = true
  }
}
