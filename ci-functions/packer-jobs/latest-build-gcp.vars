---
# Matillion ETL Installation & Configuration Example .vars file
# --------------------------------------------------------------------
# This file is to be used a a guide on how to use the ansible playbook in
# which it sits. It outlines the end to end configuration which will
# provision a Matillion ETL VM.
# --------------------------------------------------------------------

# Base OS Further Configuration
MINIMAL: true
HARDEN: true
ENHANCE: true
AUTO_SECURITY_UPDATES: true
PYTHON_VERSIONS: 

METL_MAJ_VER: 1.69

# Tomcat Installation & Configuration
CATALINA_OUT_DIRECTORY: "/var/log/tomcat"
TOMCAT_SELF_SIGNED_CERTIFICATE: true
SSL_TRUST_STORE_PATH: /etc/pki/ca-trust/extracted/java/cacerts
SSL_TRUST_STORE_PASSWORD: "changeit" # Only modify if using a custom/modified trust store, this is the default for most of the supported operating systems

# Specify where to do an install against a docker-based host
DOCKERISED_INSTALL: false

# Matillion ETL Installation & Configuration
# --------------------------------------------------------------------
INSTALL_METL: true
# --------------------------------------------------------------------
# - Matillion Version:
# If an alternative version of Matillion ETL is required, this can be
# specified here. If the version of unavailable it may need to be pulled
# from the archive repo and placed somewhere the installation machine
# can access.
# METL_VERSION:
# --------------------------------------------------------------------
# - Matillion Yum Endpoints:
# Use these variables if there is a requirement to install from an alternative repository
# such as an internal private mirror or a local file reference to the installation of
# Matillion ETL (or allow-listed internal endpoint if there is no allowed egress)
# If using a repo with basic auth credentials, ensure you construct these creds in the
# base url e.g.
# METL_UNIVERSAL_REPO: https://user:password@example.com
# METL_CDATA_REPO: https://user:password@example.com
# Is basic auth is not required, then use the following structure:
# METL_UNIVERSAL_REPO: https://example.com
# METL_CDATA_REPO: https://example.com

# Default to stable yum repository of Matillion ETL
METL_UNIVERSAL_REPO: https://artifacts.matillion.com/rpm/matillion-metl/stable/
METL_CDATA_REPO: https://artifacts.matillion.com/rpm/matillion-cdata/stable/

CONFIGURATION_MANAGEMENT_TOOL: ansible
CONFIGURATION_MANAGEMENT_REPO: https://bitbucket.org/matillion/matillion-etl-system-configuration.git
CONFIGURATION_MANAGEMENT_BRANCH: stable

METL_ENVIRONMENT_VARIABLES: "/tmp/gcp-bigquery-emerald.properties"
# --------------------------------------------------------------------
# - Matillion Metadata Configuration:
METL_METADATA_STORE: "postgres"
METL_METADATA_USERNAME: "postgres"
METL_METADATA_PASSWORD: "postgres"
# --------------------------------------------------------------------

# Postgres Metadata Store Installation & Configuration
INSTALL_LOCAL_METADATA_STORE: true

# Cloudwatch Installation and Configuration
# --------------------------------------------------------------------
# This section needs to be executed AFTER the general install if creating
# a golden image. This is because the cloudwatch configuration has region
# specific attributes in it, so needs to be set when the region for the
# running EC2 machine is known.
#
# - AWS_REGION can only be used if the installation and running machine is to be
#   within the same AWS region.
#
# - INCLUDE_CLOUDWATCH_WRAPPER adds a Cloudwatch wrapper script which can be
# called at any time post-launch (such as in cloudformation) to configure and
# setup AWS Cloudwatch logging.
#
# The instance role must have permission to the AWS Cloudwatch logging
# service to allow this to function is either regard.
# ---------------------------------------------------------------------------
AWS_REGION: null # Set to the region to execute the role against, e.g. eu-west-1
INCLUDE_CLOUDWATCH_WRAPPER: true
SETUP_CLOUDWATCH: false
CLOUDWATCH_LOG_GROUP: "Matillion-ETL"
# --------------------------------------------------------------------
