//-------------------------------------------------------------------
// These are the variables are used for the packer definition
//-------------------------------------------------------------------

variable "matillion_project_id" {
  type    = string
  default = "${env("MATILLION_GCP_PROJECT")}"
}

variable "date" {
  type    = string
  default = "${env("DATE")}"
}

//-------------------------------------------------------------------

source "googlecompute" "validation" {
  disk_size    = "50"
  account_file = "/tmp/gcp-credentials"
  source_image = "rhel-7-v20220719"
  image_name   = "metl-sys-config-delete-me-${var.date}"
  metadata = {
    enable-oslogin = "false"
  }
  project_id   = "${var.matillion_project_id}"
  ssh_pty      = "true"
  skip_create_image = "true"
  ssh_username = "packer"
  zone         = "europe-west1-b"
  machine_type = "n1-standard-2" // RHEL requires more power to run the build in an effective time
}

build {
  sources = [
    "source.googlecompute.validation", 
    ]

  // Using a wrapper script rather than copying the files straight across
  // as is more accurately simulates customer behaviour for the install
  // orchestration.
  provisioner "file" {
    source = "./orchestrate-install.sh"
    destination = "/tmp/orchestrate-install.sh"
  }

  provisioner "file" {
    source = "./ci-functions/packer-jobs/core-build.vars"
    destination = "/tmp/core-build.vars"
  }

  provisioner "file" {
    source = "./ci-functions/packer-jobs/validation-environment.vars"
    destination = "/tmp/validation-environment.vars"
  }

 
  provisioner "shell" {
    inline = ["chmod a+x /tmp/orchestrate-install.sh && sudo /bin/bash /tmp/orchestrate-install.sh /tmp/core-build.vars"]
  }

  post-processor "manifest" {
    output     = "manifest.json"
    strip_path = true
  }
}
