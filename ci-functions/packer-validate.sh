#!/bin/bash
# -------------------------------------------------------------
# Call this function with: - packer-validate.sh packerfile
# Currently this variant is the only one that is supported. 
# The automated testing will be extended to include the other
# providers and products as this repository progresses.
# -------------------------------------------------------------

# Any failure should fail this CI build
set -euo pipefail
set -x

setup_variables() {
  # Initially hardcoded for CentOS on GCP, this will be extended accordingly as our
  # automated testing progresses
  export MATILLION_GCP_PROJECT="$MATILLION_GCP_PROJECT"   # Included so it can be hardcoded if required
  export DATE=$(date +'%d-%m-%Hh-%Mm')
  export PATH=$PATH:/tmp/bin/google-cloud-sdk/bin/
  sed -i "s/CONFIGURATION_MANAGEMENT_BRANCH: stable/CONFIGURATION_MANAGEMENT_BRANCH: $CIRCLE_BRANCH/" ./ci-functions/packer-jobs/latest-build-gcp.vars
  echo -e "\nCONFIGURATION_MANAGEMENT_BRANCH: $CIRCLE_BRANCH" >> ./ci-functions/packer-jobs/core-build.vars
}

aws_cleardown() {
  echo "Info: Could potentially use this area to clear down some instances if they are hanging"
}

gcp_cleardown() {
  echo "Info: Could potentially use this area to clear down some instances if they are hanging"
}

azure_cleardown() {
  echo "Info: Clearing down Azure resources for metlsysvalidation to prevent runaway"
  az group delete --resource-group metlsysvalidation --yes || echo "Info: RG metlsysvalidation could not be deleted"
  az group create -l centralus -n metlsysvalidation --tags Category=Development SBU=Pod1 Owner=developer@matillion.com 
  az storage account create --name metlsysvalidation --resource-group metlsysvalidation -l centralus --sku Standard_RAGRS --kind StorageV2
}

main(){
  packer_file=$1

  setup_variables $packer_file

  [[ $packer_file == *aws* ]] && aws_cleardown
  [[ $packer_file == *gcp* ]] && gcp_cleardown
  [[ $packer_file == *azure* ]] && azure_cleardown

  export AWS_CONFIG_FILE=.aws_credentials && packer build ./ci-functions/packer-jobs/$packer_file
}

main $1
