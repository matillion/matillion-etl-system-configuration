#!/bin/bash

# some basic checks that our METL installation has been successful - Ansible script running successfully does not
# mean that we can actually login to the instance, after all

# Get the operating system information
os_name=$(grep '^NAME=' /etc/os-release | cut -d '"' -f 2)
os_version=$(grep '^VERSION_ID=' /etc/os-release | cut -d '"' -f 2)

# Check if the OS is Amazon Linux 2
if [[ "$os_name" == "Amazon Linux" && "$os_version" == "2" ]]; then
	pg_ctl_location=$(find /usr -name "pg_ctl")
	su postgres --command="${pg_ctl_location} -D /var/lib/pgsql/data start"
	sleep 10

else
    # start Postgres
	pg_ctl_location=$(find /usr -name "pg_ctl")
	pg_ctl_version=$(echo ${pg_ctl_location} | grep -oP '(?<=pgsql-)\d+')
	su postgres --command="${pg_ctl_location} -D /var/lib/pgsql/${pg_ctl_version}/data start"
fi



sleep 10

# start METL
/opt/tomcat/bin/catalina.sh start
sleep 10

if [ -f /var/log/tomcat/catalina.out ]; then 
	CATALINA_OUTPUT=/var/log/tomcat/catalina.out
else
	CATALINA_OUTPUT=/opt/tomcat/logs/catalina.out
fi


# does METL startup in a reasonable time with the expected message?
ATTEMPT_COUNTER=0
MAX_RETRIES=60 # in multiples of ten seconds

head --lines 15 "${CATALINA_OUTPUT}"

while true
do

	if [ $( grep "Server startup in" "${CATALINA_OUTPUT}" | wc -l ) -eq 1 ]; then
		echo "Server startup successful at $( date )"
		break
	fi

	ATTEMPT_COUNTER=$(( ATTEMPT_COUNTER + 1 ))
	if [ ${ATTEMPT_COUNTER} -eq ${MAX_RETRIES} ]; then
		echo "Server failed to start"
		cat "${CATALINA_OUTPUT}"
		exit 1
	fi

	echo "Still waiting for server startup at $( date ) - attempt ${ATTEMPT_COUNTER}"
	sleep 10

done


# have login credentials been created?
if [ "$( grep ec2-user /etc/tomcat/tomcat-users.xml )" ]; then
	echo "Default ec2-user appears to have been created"
else
	echo "No ec2-user appears to have been created"
	exit 1
fi


# can we connect on http?
if [ "$( curl http://localhost:8080 | grep 'Matillion ETL' )" ]; then
	echo "Server seems to be responding with HTTP on 8080"
else
	echo "Server does not seem to be responding with HTTP on 8080"
	exit 1
fi


# can we connect on https?  (--insecure, because we're self-signed)
if [ "$( curl --insecure https://localhost:8443 | grep 'Matillion ETL' )" ]; then
	echo "Server seems to be responding with HTTPS on 8443"
else
	echo "Server does not seem to be responding with HTTPS on 8443"
	exit 1
fi
