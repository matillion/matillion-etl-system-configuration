#!/bin/bash

# Call this function with: 
# ./orchestrate-install.sh <filepath to variables>

set -eo pipefail

VARIABLES_FILE="${1:-/tmp/installation.vars}"

check_free_disk_space(){
	
	echo "------------------------------------"
	echo "Checking free space for root and var mount points"
	
	directory=$1
    space_required=$2

	# Check if mount point exists
	if [ -d "$directory" ]; then
		# Get size of mount point in gigabytes
		directory_size=$(df --block-size=G $directory | awk 'NR==2 {print $4}' | sed 's/G//')

		# Check if directory size is smaller than required size
		if [ "$directory_size" -lt $space_required ]; then
			echo "Error: $directory has less than $space_required of available space."
			exit 1
		fi
	fi

	echo "$directory mount point does have sufficient space."
	echo "---------------------------------"

}

check_params_set(){
	if [[ -z "$VARIABLES_FILE" ]]; then
		echo "Error: A variables file needs to be passed to this installation wrapper for it to succeed. Please invoke with ./orchestrate-install.sh <filepath>" 
		exit 1
	fi
	echo "---------------------------------"
	echo "Values passed as inputs:"
	echo "VARIABLES_FILE=$VARIABLES_FILE"
	cat $VARIABLES_FILE
	echo "---------------------------------"

	return 0
}

check_system_release() {
	echo "------------------------------------"
	echo "INFO: /etc/system-release shown below:"
	cat /etc/system-release
	echo "System is: $( uname -a )"
	echo "------------------------------------"

	[[ "$(cat /etc/system-release | grep '\<Amazon Linux release 2\>')" ]] && export OS_VARIANT=amazon-linux-2 && return 0
	[[ "$(cat /etc/system-release | grep 'CentOS Linux release 7')" ]] && export OS_VARIANT=centos-7 && return 0 
	[[ "$(cat /etc/system-release | grep 'CentOS Stream release 8')" ]] && export OS_VARIANT=centos-stream-8 && return 0 
	[[ "$(cat /etc/system-release | grep 'CentOS Stream release 9')" ]] && export OS_VARIANT=centos-stream-9 && return 0 
	[[ "$(cat /etc/system-release | grep 'Red Hat Enterprise Linux Server release 7')" ]] && export OS_VARIANT=redhat-7 && return 0
	[[ "$(cat /etc/system-release | grep 'Red Hat Enterprise Linux release 8')" ]] && export OS_VARIANT=redhat-8 && return 0
	[[ "$(cat /etc/system-release | grep 'Red Hat Enterprise Linux release 9')" ]] && export OS_VARIANT=redhat-9 && return 0
	[[ "$(cat /etc/system-release | grep 'Rocky Linux release 8')" ]] && export OS_VARIANT=rocky-8 && return 0
	[[ "$(cat /etc/system-release | grep 'Rocky Linux release 9')" ]] && export OS_VARIANT=rocky-9 && return 0

	echo "Unrecognised and unsupported OS variant - proceeding at your own risk"
	export OS_VARIANT=other && return 0
}

# get a value for a variable in order of preference (a) from the environment (b) from the variables file (c) using the default provided.
# this helps with running this script in pipelines
get_or_default() {
	VARIABLE_NAME=$1
	DEFAULT_VALUE=$2

	if [[ -v "$VARIABLE_NAME" ]]; then # it's defined as an environment variable
		echo "${!VARIABLE_NAME}"
		return 0
	fi

	if [[ $( grep --count --regexp "^${VARIABLE_NAME}:" "${VARIABLES_FILE}" ) == 1 ]]; then # it's defined in our variables file
		echo "$(sed --silent --expression "s%^${VARIABLE_NAME}: %%p" ${VARIABLES_FILE})"
		return 0
	fi
	
	# default value it is.
	echo "${DEFAULT_VALUE}"
	return 0
}

postgresql_repo_config(){
    POSTGRES_URL=$1
    DISABLE_DNF_MODULE=$2
    POSTGRESQL_VERSION=${POSTGRESQL_VERSION:-16} 

    dnf --verbose install --assumeyes "${POSTGRES_URL}"

    if [[ "$DISABLE_DNF_MODULE" == true ]]; then
        dnf module disable --assumeyes postgresql || true
    fi

    if [[ "$POSTGRESQL_VERSION" -gt 15 ]]; then
        yum-config-manager --disable pgdg12 pgdg13 pgdg14 pgdg15 pgdg16 pgdg17
		yum-config-manager --enable pgdg16
    else
        yum-config-manager --disable pgdg12 pgdg13 pgdg14 pgdg15 pgdg16 pgdg17
        yum-config-manager --enable pgdg"$POSTGRESQL_VERSION"
    fi
}


check_or_error(){
    VARIABLE_NAME=$1
    REGEX_EXPRESSION=$2

    VARIABLE_VALUE=${!VARIABLE_NAME}

    # Check if the value matches the regex
    if [[ ! $VARIABLE_VALUE =~ $REGEX_EXPRESSION ]]; then
        echo "Error: Value of $VARIABLE_NAME ('$VARIABLE_VALUE') does not match the expected pattern."
        return 1
    else
        echo "Value of $VARIABLE_NAME ('$VARIABLE_VALUE') is valid."
        return 0
    fi
}

# check whether certain values have been defined - this is also where sensible default values can be kept
assess_configuration_management_selection() {
	export ADDITIONAL_YUM_PACKAGES="$( get_or_default ADDITIONAL_YUM_PACKAGES 'vim wget' )"
	export AUTO_SECURITY_UPDATES="$( get_or_default AUTO_SECURITY_UPDATES false )"
	export CONFIGURATION_MANAGEMENT_BRANCH="$( get_or_default CONFIGURATION_MANAGEMENT_BRANCH stable )"
	export CONFIGURATION_MANAGEMENT_REPO="$( get_or_default CONFIGURATION_MANAGEMENT_REPO https://bitbucket.org/matillion/matillion-etl-system-configuration.git )"
	export CONFIGURATION_MANAGEMENT_TOOL="$( get_or_default CONFIGURATION_MANAGEMENT_TOOL ansible )"
	export INSTALL_METL="$( get_or_default INSTALL_METL true )"
	export POSTGRESQL_VERSION="$( get_or_default POSTGRESQL_VERSION 16 )"
	export PYTHON_VERSIONS="$( get_or_default PYTHON_VERSIONS '2.7.18 3.12.4' )"
	export TOMCAT_10_VERSION="$( get_or_default TOMCAT_10_VERSION 10.1.36 )"
	export TOMCAT_8_VERSION="$( get_or_default TOMCAT_8_VERSION 8.5.98 )"
	export METL_MAJ_VER="$( get_or_default METL_MAJ_VER 1.75 )"
	export SKIP_DISK_CHECKS="$( get_or_default SKIP_DISK_CHECKS false )"
	export USE_JAVA_REPOSITORY="$( get_or_default USE_JAVA_REPOSITORY false )"
	export SKIP_SELINUX="$( get_or_default SKIP_SELINUX false )"
	export OPTIONAL_PYTHON3_MODULES="$( get_or_default OPTIONAL_PYTHON3_MODULES '')"

}

install_pre_reqs() {
	echo "Installing prereqs for $OS_VARIANT"
	set -x

	yum --verbose clean all
	yum --verbose update --assumeyes

	# python3 + pip are required to run Ansible; Git is required to clone the installation repo
	YUM_REQUIREMENTS="${ADDITIONAL_YUM_PACKAGES} python3 python3-pip git sudo tar"
    export PYTHON3_BUILD_DEPENDENCIES="bzip2-devel gcc libffi-devel make openssl-devel zlib-devel"
	# all OS-specific configuration is done here; mostly just enabling Extra Packages for Enterprise Linux, and a couple of packages surprisingly not installed by default
	case $OS_VARIANT in

	amazon-linux-2)
		export POSTGRESQL_VERSION=13 # Latest supported version
		amazon-linux-extras enable postgresql$POSTGRESQL_VERSION epel --assumeyes
		export YUM_REQUIREMENTS="$YUM_REQUIREMENTS tar-*" # needed to unpack Tomcat
		yum remove openssl -y
		yum install openssl11 -y
		ln /bin/openssl11 /bin/openssl
		export PYTHON3_BUILD_DEPENDENCIES="bzip2-devel gcc libffi-devel make openssl11-devel zlib-devel"
		;;

	centos-7)
		yum --verbose install --assumeyes epel-release
		;;

	centos-stream-8 )
		yum install --assumeyes dnf-plugins-core yum-utils
		dnf --verbose config-manager --set-enabled powertools
		dnf --verbose install --assumeyes epel-release epel-next-release
		curl https://archive.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-8 --output /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-8
		rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-8
		postgresql_repo_config "https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm" true 
		;;

	centos-stream-9 )
		yum install --assumeyes dnf-plugins-core
		dnf --verbose install --assumeyes epel-release epel-next-release yum-utils
		curl https://archive.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-8 --output /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-8
		rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-8
		postgresql_repo_config "https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm" true
		;;

	rocky-8)
		yum install --assumeyes dnf-plugins-core yum-utils findutils
		dnf --verbose config-manager --set-enabled powertools
		dnf --verbose install --assumeyes epel-release
		curl https://archive.fedoraproject.org/pub/epel/RPM-GPG-KEY-EPEL-8 --output /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-8
		rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-8
		postgresql_repo_config "https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm" true
		;;
	
	rocky-9)
		yum install --assumeyes dnf-plugins-core yum-utils
		dnf --verbose install --assumeyes https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm
		dnf --verbose install --assumeyes https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm
		postgresql_repo_config "https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm" true
		;;

	redhat-7)
		# Yum returns 0 for 'installed successfully', and 1 for 'no need to do anything'; silence that and see if we can continue...
		yum --verbose install --assumeyes https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm || echo "Yum reported error; continuing..."
		yum --verbose install --assumeyes yum-utils
		postgresql_repo_config "https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm" false
		;;

	redhat-8)
		dnf --verbose install --assumeyes yum-utils
		dnf --verbose install --assumeyes https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
		postgresql_repo_config "https://download.postgresql.org/pub/repos/yum/reporpms/EL-8-x86_64/pgdg-redhat-repo-latest.noarch.rpm" true
		;;

	redhat-9)
		dnf --verbose install --assumeyes yum-utils
		dnf --verbose install --assumeyes https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm
		postgresql_repo_config "https://download.postgresql.org/pub/repos/yum/reporpms/EL-9-x86_64/pgdg-redhat-repo-latest.noarch.rpm" false
		;;

	*)
		echo "Unrecognised base operating system - proceeding at your own risk..."
		# We get very few 'OS' issues - possibly none? - and certainly they pale in comparison to problems caused by other configuration problems.
		# If your chosen OS is RPM-based, then there's a good chance that METL will install on it - indeed, most of our devs use DEB-based systems.
		# If we don't have experience with your OS (ie. it's not in this list) do understand there will be a limit to the support we can provide on it - time and expertise will be limited.
		;;

	esac

	yum --verbose install --assumeyes $YUM_REQUIREMENTS

	# fix the interpreter used for Ansible to 'what Python is now', as we're likely to update Python during install and Ansible gets very confused
	export ANSIBLE_PYTHON_INTERPRETER=/usr/bin/python3
	while [[ -n "$( readlink "$ANSIBLE_PYTHON_INTERPRETER" )" ]]; do
		export ANSIBLE_PYTHON_INTERPRETER=$( readlink $ANSIBLE_PYTHON_INTERPRETER )
	done
	export ansible_python_interpreter=$ANSIBLE_PYTHON_INTERPRETER

	echo "Using $ANSIBLE_PYTHON_INTERPRETER for Ansible installation"
	$ANSIBLE_PYTHON_INTERPRETER -m pip install --upgrade pip || echo "Could not upgrade Python3-Pip - assuming it is system managed" # specifically, AL2003 has this problem
	$ANSIBLE_PYTHON_INTERPRETER -m pip install --upgrade setuptools-rust ansible

	set +x
}

pull_configuration_management() {
	RANDOM_NUMBER=$RANDOM
	echo "Installation folder set to /tmp/matillion-etl-install-$RANDOM_NUMBER"
	mkdir -p /tmp/matillion-etl-install-$RANDOM_NUMBER && cd /tmp/matillion-etl-install-$RANDOM_NUMBER
	git clone $CONFIGURATION_MANAGEMENT_REPO
	cd matillion-etl-system-configuration
	echo "Checking out branch: $CONFIGURATION_MANAGEMENT_BRANCH"
	git checkout $CONFIGURATION_MANAGEMENT_BRANCH
}

execute_configuration_management() {
	if [[ $CONFIGURATION_MANAGEMENT_TOOL == "ansible" ]]; then
		ansible_location=$(whereis ansible | awk '{print $2}')
		ansible_playbook_location=$(whereis ansible-playbook | awk '{print $2}')
		ansible_galaxy_location=$(whereis ansible-galaxy | awk '{print $2}')
		echo "Ansible python interpeter location is" $ANSIBLE_PYTHON_INTERPRETER
		echo "Info: Running config management against $($ansible_location --version | head -n 1)"
		$ansible_playbook_location -vv -i ./Matillion-ETL/ansible/hosts.yaml --connection=local ./Matillion-ETL/ansible/main.yaml --extra-vars @$VARIABLES_FILE
		echo "Info: Please reboot the machine by entering the command 'reboot' or by using the relevant cloud provider console."
		echo "Info: Continue to follow the instructions within the readme of matillion-system-configuration.git, the service will serve on port 80 & 443 in a few minutes after reboot."
	else
		# don't know why we make this an option, really
		echo "Error: Unsupported or unknown configuration management tool specified, exiting."
		exit 1
	fi
}

execute_cleanup() {
	python3 -m pip uninstall --yes ansible ansible-core setuptools-rust 
	if grep -q "Amazon Linux 2" /etc/os-release; then
    	# Amazon Linux 2
    	yum remove --assumeyes git --skip-broken
	else
		# Other OS
		yum remove --assumeyes git --skip-broken --noautoremove
	fi
	rm -Rf /tmp/matillion-etl-install*
	yum clean all
}

METL_MAJ_VER_REGEX="^[0-9]+\.[0-9]{2}$"

check_params_set && echo -e "Installation Values found to be:\n - $VARIABLES_FILE"
check_system_release && echo "Operating System found to be $OS_VARIANT"
assess_configuration_management_selection

if [[ "$SKIP_DISK_CHECKS" != "true" ]]; then
	check_free_disk_space '/' '20'
	check_free_disk_space '/var' '20'
	check_free_disk_space '/usr' '20'
	check_free_disk_space '/var/log' '10'
else
	echo "Skipping disk space checks as per configuration."
fi

check_or_error "METL_MAJ_VER" $METL_MAJ_VER_REGEX
install_pre_reqs
pull_configuration_management
execute_configuration_management
execute_cleanup
